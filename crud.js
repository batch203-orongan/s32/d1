let http = require("http");

const port = 4000;

// Mock Database
let directory = [
	{
		name:"Brandon",
		email:"brandon@mail.com"
	},
	{
		name:"Jobert",
		email:"jobert@mail.com"
	}
]

const server = http.createServer((request, response) => {

	// When the '/users' route is accessed with a method of "GET" we will send the directory (mock database) list of users.

	if(request.url == "/users" && request.method == "GET"){

		response.writeHead(200, {"Content-Type": "application/json"});

		response.write(JSON.stringify(directory));

		response.end();
	}

	// We want to recieved the content of the request and save it to the mock database.
		// Content will be retrive from the request bosy.
		// Parse the recieved JSON request body to JavaScript Object.
		// Add the parse object to the directory (mock database).
	if(request.url == "/users" && request.method == "POST"){

		// This will act as a placeholder for the resource/data to be created later on.
		let requestBody = "";

		// data step - this reads "data" stream and process is as a request
		request.on("data", (data) => {
			// data recieved is on chunk of information
			console.log(data);

			// Assigns the data retrieved form the data to stream to requestBody.
			// at this pont, "requestBody" has the 
			requestBody += data;
			// to show that the chunk of information is stored in the requestBody.
			console.log(requestBody);
		})

		//response end step - only runs after the erqueest has completely sent.
		request.on("end", ()=> {
			console.log(typeof requestBody);

			requestBody = JSON.parse(requestBody);

			console.log(typeof requestBody);

			directory.push(requestBody);
			console.log(directory);

			response.writeHead(200, {"Content-Type": "application/json"});
			response.write(JSON.stringify(requestBody));
			response.end();
		});
	}

});

server.listen(port);

console.log(`Server is running at localhost:${port}`)